﻿using Kara.CMS.Business.Processing;
using Kara.CMS.Entities;
using Kara.CMS.Web.Common.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace Kara.CMS.Web.Common.Controllers
{
    [CmsFilter]
    public class BaseController : System.Web.Mvc.Controller
    {
        public void SetSuccessNotification(string message, bool setElement = false)
        {
            ClearNotifications();
            if (setElement) TempData["Notification.Success"] = message;
            SetJsNotitfication(message, "success");
        }
        public void SetErrorNotification(string message, bool setElement = false)
        {
            ClearNotifications();
            if (setElement) TempData["Notification.Error"] = message;
            SetJsNotitfication(message, "error");
        }
        public void SetInfoNotification(string message, bool setElement = false)
        {
            ClearNotifications();
            if (setElement) TempData["Notification.Info"] = message;
            SetJsNotitfication(message, "info");
        }
        public void SetWarningNotification(string message, bool setElement = false)
        {
            ClearNotifications();
            if (setElement) TempData["Notification.Warning"] = message;
            SetJsNotitfication(message, "warning");
        }
        private void ClearNotifications()
        {
            TempData["Notification.Success"] = null;
            TempData["Notification.Error"] = null;
            TempData["Notification.Info"] = null;
            TempData["Notification.Warning"] = null;
        }
        private void SetJsNotitfication(string message, string type)
        {
            ViewBag.Notifications = string.Format("Global.DisplayMessage('{0}', '{1}');", message, type);
        }

        public Section GetSectionAndWidgets(int id)
        {
            var prosessing = new ContentProcessing();
            return  prosessing.GetSectionAndWidgets(s => s.SectionID == id).FirstOrDefault();
        }
    }
}