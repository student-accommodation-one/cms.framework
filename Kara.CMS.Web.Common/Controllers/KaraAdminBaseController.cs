﻿using Kara.Framework.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Kara.CMS.Web.Common.Controllers
{
    public class KaraAdminBaseController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Generate()
        {
            var input = Request["input"];
            var sha1 = new SHA1CryptoServiceProvider();
            return Content(BitConverter.ToString(sha1.ComputeHash(Encoding.Default.GetBytes(input))));
            //var helper = new MachineKeyHelper();
            //var protectedText = helper.Protect(input);
            //var reverseTest = helper.Unprotect(protectedText);
            //return Content(protectedText); 
        }
    }
}
