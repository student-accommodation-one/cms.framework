﻿using AutoMapper;
using Kara.CMS.Business;
using Kara.CMS.Business.Processing;
using Kara.CMS.Common.Enums;
using Kara.CMS.Entities;
using Kara.CMS.Web.Common.Filters;
using Kara.CMS.Web.Common.Models;
using Kara.Framework.Common;
using Kara.Framework.Common.Entity;
using Kara.Framework.Common.Mvc.ActionResults;
using Kara.Framework.Common.Mvc.Filters;
using Kara.Framework.Common.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Kara.CMS.Web.Common.Controllers
{
    [JsControlFilter]
    [NoCache]
    public class BaseCmsController : BaseController
    {
        //
        // GET: /Cms/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContentItems()
        {
            return View();
        }

        public ActionResult Pages()
        {
            return View();
        }

        [JsGridGetRequestFilter]
        public ActionResult ContentItems_Get()
        {
            var contentProsessing = new ContentProcessing();
            var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
            var result = contentProsessing.GetContentItems(null, jsGridRequest);
            var viewResult = new ActionResultCustom<ContentItemViewModel>()
            {
                IsSuccess = result.IsSuccess,
                Messages = result.Messages,
                TotalRows = result.TotalRows
            };

            var mapped = Mapper.Map<List<ContentItemViewModel>>(result.Results);
          
            mapped.ForEach(m => m.ThumbnailPath = string.IsNullOrWhiteSpace(m.ThumbnailPath) ? 
                WebPathSettings.ToRootRelativePath(WebPathSettings.NotAvailableImagePath()) :
                WebPathSettings.ToRootRelativePath(m.ThumbnailPath));
            viewResult.Results = mapped;


            return Json(viewResult, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ContentItems_Update(List<ContentItemViewModel> models, List<string> tests)
        {
            var modelAreValid = ModelState.IsValid;
            var result = new ActionResultCustom<string>(); 

            if (ModelState.IsValid)
            {
                var processing = new ContentItemProcessing();
                var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
                var contentItems = Mapper.Map<List<ContentItem>>(models);
                result = processing.UpdateContentItems(contentItems);
              
            }
            else
            {
                result.IsSuccess = false;
               
                for (int i = 0; i < ModelState.Count; i++)
			    {
                    var key = ModelState.Keys.ElementAt(i);
                    var modelIndex = int.Parse(key.Split(new char[] { ']'})[0].Replace(@"[",""));
                    var modelState = ModelState.Values.ElementAt(i);
                    			 
                    foreach (ModelError error in modelState.Errors)
                    {
                        result.Messages.Add(new ActionResultCustomMessage(string.Format("{0} - {1}", models.ElementAt(modelIndex).ContentItemId, error.ErrorMessage)));
                    }
                }
                
            }
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ContentItems_Upload()
        {
            if (this.HttpContext.Request.Files.Count > 0)
            {
                var processing = new ContentItemProcessing();
                var result = processing.UploadContentItems(this.HttpContext.Request.Files);
                return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        [HttpPost]
        public ActionResult ContentItems_Delete(List<ContentItemViewModel> models)
        {
            var processing = new ContentItemProcessing();
            var contentItems = Mapper.Map<List<ContentItem>>(models);
            var result = processing.DeleteContentItems(contentItems);
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }
        
        [JsGridGetRequestFilter]
        public ActionResult PageSectionList()
        {
            var contentProsessing = new ContentProcessing();
            var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
            var result = contentProsessing.GetPageSectionView(null, jsGridRequest);
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Section(int id)
        {           
            var prosessing = new ContentProcessing();
            var result = prosessing.GetSections(s => s.SectionID == id).FirstOrDefault();
            var mapped = Mapper.Map<SectionDetailsViewModel>(result);
            return View(mapped);
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Section(SectionDetailsViewModel model)
        {           
            ModelState.Clear();
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveSection(SectionDetailsViewModel model)
        {
            var processing = new ContentProcessing();
            var section = processing.GetSections(s => s.SectionID == model.SectionID).FirstOrDefault();
            if (section != null)
            {                
                section.ContentTypeID = model.ContentTypeId;
                if (section.ContentTypeID == (int) ContentTypes.TEXT)
                {
                    section.Content = HttpContext.Server.HtmlEncode(model.Content);
                }
                else
                {
                    section.Content = model.Content;
                }
                
                section.LastUpdatedDate = DateTime.UtcNow;
                section.LastUpdatedBy = HttpContext.User.Identity.Name;
                processing.SaveSections(new List<Section>(){section});
                return RedirectToAction("Pages");
            }
           
            return View(model);
        }

        public ActionResult Images()
        {
            //{"name":"a100.png","type":"f","size":73289}
            var contentProsessing = new ContentProcessing();
            //Cannot order desc, because the control has its own logic
            var result = contentProsessing.GetContentItems(c => c.ContentTypeID == (int)ContentTypes.IMAGE, new EntityDataRequest());
            var jsonResultList = new List<ExpandoObject>();
            foreach (var item in result.Results)
            {
                dynamic jsonResult = new ExpandoObject();
                jsonResult.itemId = item.ContentItemID;
                jsonResult.name =  item.FileName;
                jsonResult.type = "f";
                jsonResult.size = item.FileSize;
                jsonResultList.Add(jsonResult);
            }

            var jsonNetResult = new JsonNetResult();
            jsonNetResult.Data = jsonResultList;
            return jsonNetResult;
        }

        public ActionResult Files()
        {
            var contentProsessing = new ContentProcessing();
            //Cannot order desc, because the control has its own logic
            var result = contentProsessing.GetContentItems(c => c.ContentTypeID == (int)ContentTypes.APPLICATION, new EntityDataRequest());
            var jsonResultList = new List<ExpandoObject>();
            foreach (var item in result.Results)
            {
                dynamic jsonResult = new ExpandoObject();
                jsonResult.itemId = item.ContentItemID;
                jsonResult.name = item.FileName;
                jsonResult.type = "f";
                jsonResult.size = item.FileSize;
                jsonResultList.Add(jsonResult);
            }

            var jsonNetResult = new JsonNetResult();
            jsonNetResult.Data = jsonResultList;
            return jsonNetResult;
        }
        public ActionResult Thumbnail(string path)
        {
            var contentItemId = ContentItem.ParseContentItemID(path);
            var processing = new ContentProcessing();
            var result = processing.GetContentItems(c => c.ContentItemID == contentItemId, new EntityDataRequest());
            var contentItem = result.Results.FirstOrDefault();
            if (contentItem != null)
            {
                 var contentType = new LookupProcessing().GetLuContentTypes().Results.First(l => l.ContentTypeID == (int)ContentTypes.IMAGE);
                 var virPath = contentItem.ThumbnailPath;
                 return File(System.Web.HttpContext.Current.Server.MapPath(virPath),contentItem.FileContentType);
            }

            return null;            
        }




        /*********************** ADD WIDGET PAGES **************************************/

        public ActionResult WidgetGallery(int id)
        {
            var processing = new ContentProcessing();
            var section = processing.GetSections(s => s.SectionID == id).FirstOrDefault();

            return View(section);
        }

        [JsGridGetRequestFilter]
        public ActionResult WidgetGalleryList(int id)
        {
            var processing = new WidgetGalleryProcessing();
            var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
            var result = processing.GetWidgetGalleryView(s => s.SectionID == id, jsGridRequest);
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        public ActionResult WidgetGalleryItems(int id)
        {
            var processing = new WidgetGalleryProcessing();
            var result = processing.GetWidgetGalleries(w => w.WidgetGalleryID == id);
            return View(result.Results.FirstOrDefault());
        }

        [JsGridGetRequestFilter]
        public ActionResult WidgetGalleryItemsList(int id)
        {
            var processing = new WidgetGalleryProcessing();
            var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
            var result = processing.GetWidgetGalleryItemsView(w => w.WidgetGalleryID == id, jsGridRequest);

            foreach (var item in result.Results)
                item.ThumbnailPath = WebPathSettings.ToRootRelativePath(item.ThumbnailPath);
                    
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        public ActionResult WidgetGalleryDelete(List<VwWidgetGalleryItem> models)
        {
            var processing = new WidgetGalleryProcessing();
            var result = processing.DeleteWidgetGallery(models.Select(m => m.WidgetGalleryID).ToList());                    
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult SaveNewWidgetGalleryFolder(WidgetGallery widgetGallery)
        {

            if (ModelState.IsValid)
            {
                var processing = new WidgetGalleryProcessing();
                widgetGallery.LastUpdatedBy = User.Identity.Name;
                widgetGallery.LastUpdatedDate = DateTime.UtcNow;

                var result = processing.SaveWidgetGalleries(new List<WidgetGallery>() { widgetGallery });
                SetSuccessNotification("Saved");
                return PartialView("_CmsNewWidgetGalleryFolderSuccess");
            }

            SetWarningNotification("Invalid data");
            return PartialView("_CmsNewWidgetGalleryFolder", widgetGallery);
        }
        

        public ActionResult NewWidgetGalleryFolder(int id)
        {
            var gallery = new WidgetGallery();
            gallery.SectionID = id;
            return PartialView("_CmsNewWidgetGalleryFolder", gallery);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WidgetGalleryItemSave()
        {
            var fileName = Request["newGalleryItemName"];
            int widgetGalleryID;
            int.TryParse(Request["WidgetGalleryID"].ToString(), out widgetGalleryID);
            if (!string.IsNullOrWhiteSpace(fileName) && widgetGalleryID > 0)
            {
                var fileNameFragments = fileName.Split(Char.Parse("_"));

                var newItem = new WidgetGalleryItem()
                {
                    WidgetGalleryID = widgetGalleryID,
                    ContentItemID = int.Parse(fileNameFragments[0]),
                    LastUpdatedBy = User.Identity.Name,
                    LastUpdatedDate = DateTime.UtcNow
                };

                var processing = new WidgetGalleryProcessing();
                var result = processing.SaveWidgetItems(new List<WidgetGalleryItem>() { newItem });

                return PartialView("_CmsNewWidgetGalleryItemSuccess");


            }
            return Content("Unable to add file");
        }

        [HttpPost]
        public ActionResult WidgetGalleryItems_Delete(List<VwWidgetGalleryItem> models)
        {
            var processing = new WidgetGalleryProcessing();
            var result = processing.DeleteWidgetGalleryItems(models);
            return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogOff()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
        #region "Widget Category Contents"
        //public ActionResult WidgetCategoryContents(int id)
        //{
        //    var processing = new ContentProcessing();
        //    var section = processing.GetSections(s => s.SectionID == id).FirstOrDefault();

        //    return View(section);
        //}

        //public ActionResult WidgetCategoryContentDetails(int id)
        //{
        //    var processing = new WidgetCategoryContentProcessing();
        //    WidgetCategoryContent widget = new WidgetCategoryContent();
        //    if (id != 0)
        //        widget = processing.GetWidgetCategoryContents(w => w.WidgetCategoryContentID == id).Results.FirstOrDefault();

        //    return PartialView("_CmsWidgetCategoryContentDetails", widget);
        //}

        //[JsGridGetRequestFilter]
        //public ActionResult WidgetCategoryContentList(int sectionId)
        //{
        //    var processing = new WidgetCategoryContentProcessing();
        //    var jsGridRequest = ViewBag.JsGridGetRequest as JsGridGetRequest;
        //    var result = processing.GetWidgetCategoryContentView(s => s.SectionID == sectionId, jsGridRequest);
        //    return Json(result, "text/x-json", JsonRequestBehavior.AllowGet);
        //}

        //[ValidateInput(false)]
        //public ActionResult WidgetCategoryContentSave(WidgetCategoryContent widget)
        //{
        //    var processing = new WidgetCategoryContentProcessing();
        //    var result = processing.SaveWidgetCategoryContents(new List<WidgetCategoryContent>() { widget });

        //    if (ModelState.IsValid)
        //    {
        //        SetSuccessNotification("Saved");
        //        return PartialView("_CmsWidgetCategoryContentDetailsSuccess");
        //    }
        //    SetErrorNotification("Invalid data");
        //    return PartialView("_CmsWidgetCategoryContentDetails", widget);
        //}        
        #endregion

        
    }
}
