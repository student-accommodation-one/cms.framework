﻿using Kara.CMS.Business.Processing;
using Kara.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Kara.CMS.Web.Common.Filters
{
    public class CmsFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
 	        base.OnActionExecuting(filterContext);
            var cmsId = CmsIdFactory.Create(filterContext.Controller.ToString(), filterContext.ActionDescriptor.ActionName);
            var page = new ContentProcessing().GetPagesAndSections(p => p.CmsID == cmsId).FirstOrDefault();
            filterContext.Controller.ViewData.Model = page;
        }       
    }
}
