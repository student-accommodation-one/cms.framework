﻿using Kara.CMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Web.Common.Models
{
    public class ContentItemViewModel : IValidatableObject
    {
        [Range(100, int.MaxValue)]
        public int ContentItemId { get; set; }

        public int ContentTypeId { get; set; }
        public string ContentTypeDescription { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Extension { get; set; }
        [Required]
        public string Path { get; set; }
        public string ThumbnailPath { get; set; }

        [Required]
        public DateTime LastUpdatedDate { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //TODO: For tighter validation can validate Extension
            return new List<ValidationResult>();
        }
    }
}
