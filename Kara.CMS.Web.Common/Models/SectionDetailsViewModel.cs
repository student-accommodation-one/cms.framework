﻿using AutoMapper;
using Kara.CMS.Business.Processing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kara.CMS.Web.Common.Models
{
    public class SectionDetailsViewModel
    {
        [Required]
        public int SectionID { get; set; }

        [Required]
        public string PageName { get; set; }

        [Required]
        public string SectionName { get; set; }

        [Required]
        public int ContentTypeId { get; set; }

        public string Content { get; set; }

        public List<SelectListItem> GetContentTypes()
        {
            var selectList = new List<SelectListItem>();
            var processing = new LookupProcessing();
            var result = processing.GetLuContentTypes().Results.Where(l => l.IsSectionEditorAvailable == true).ToList();
            return Mapper.Map(result, selectList);
        }

      
    }
}