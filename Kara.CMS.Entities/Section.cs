//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kara.CMS.Entities
{
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    public partial class Section
    {
        public Section()
        {
            this.WidgetGalleries = new HashSet<WidgetGallery>();
        }
    
        [Key]
    	 public int SectionID { get; set; }
        [Required]
    	[StringLength(100)]
    	 public string CmsID { get; set; }
        [Required]
    	 public int PageID { get; set; }
        	[StringLength(50)]
    	 public string Name { get; set; }
        [Required]
    	 public int ContentTypeID { get; set; }
         public string Content { get; set; }
        [Required]
    	 public byte RecordState { get; set; }
         public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        	[StringLength(50)]
    	 public string LastUpdatedBy { get; set; }
    
        public virtual Page Page { get; set; }
        public virtual ICollection<WidgetGallery> WidgetGalleries { get; set; }
        public virtual LuContentType LuContentType { get; set; }
    }
}
