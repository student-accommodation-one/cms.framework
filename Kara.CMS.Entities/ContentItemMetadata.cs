﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Entities
{
    public partial class ContentItem
    {

        public string GenerateFileName(bool isThumbnail = false)
        {
            var format = "{0}_{1}.{2}";
            if (isThumbnail) format = "{0}_{1}t.{2}";            

            return string.Format(format, ContentItemID, System.IO.Path.GetFileNameWithoutExtension(OriginalFileName),Extension.ToLower());
        }

        public static int ParseContentItemID(string fileName)
        {
            var underscoreIndex = System.IO.Path.GetFileNameWithoutExtension(fileName).IndexOf("_");
            return int.Parse(fileName.Substring(0, underscoreIndex));
        }
    }
}
