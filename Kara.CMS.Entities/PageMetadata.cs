﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Entities
{
    [MetadataType(typeof(PageMetadata))]
    public partial class Page
    {

    }
    public partial class PageMetadata
    {

        [UIHint("_SectionDisplay")]
        public virtual ICollection<Section> Sections { get; set; }

    }
}
