//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kara.CMS.Entities
{
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    public partial class VwWidgetGallery
    {
        [Key]
    [Required]
    	 public int SectionID { get; set; }
        [Key]
    [Required]
    	 public int WidgetGalleryID { get; set; }
        [Key]
    [Required]
    	[StringLength(50)]
    	 public string GalleryTitle { get; set; }
         public Nullable<int> Total { get; set; }
        	[StringLength(50)]
    	 public string SectionName { get; set; }
        [Key]
    [Required]
    	[StringLength(50)]
    	 public string PageName { get; set; }
    }
}
