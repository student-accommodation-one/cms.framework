﻿using Kara.Framework.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Repository
{
    public class KaraCmsUnitOfWork : UnitOfWork
    {
        public KaraCmsUnitOfWork() : base(new KaraCMSEntities())  {
            this.DbContext.Configuration.LazyLoadingEnabled = true;
            this.DbContext.Configuration.ProxyCreationEnabled = false;
        }

    }
}
