﻿using Kara.CMS.Entities;
using Kara.Framework.Common;
using Kara.Framework.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Repository
{
    public partial class ContentItemRepository
    {
        public ActionResultCustom<string> UpdateContentItems(List<ContentItem> contentItems)
        {
            var result = new ActionResultCustom<string>();
            foreach(var item in contentItems)
            {
                DbContext.Database.ExecuteSqlCommand("UPDATE dbo.ContentItem SET Description = @Description WHERE ContentItemID = @ContentItemID", 
                    new SqlParameter("@Description", item.Description),
                    new SqlParameter("@ContentItemID", item.ContentItemID));
            }
            result.IsSuccess = true;
            return result;
        }
    }
}
