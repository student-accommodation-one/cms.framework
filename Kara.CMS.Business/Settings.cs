﻿using Kara.CMS.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kara.CMS.Business
{
    public class WebPathSettings
    {
        public static string GetContentItemVirtualPath(LuContentType contentType, ContentItem contentItem, bool thumbnail)
        {
            var fileName = thumbnail ? contentItem.GenerateFileName(true) : contentItem.FileName;
            var virPath = string.Format("~/{0}/{1}", contentType.DefaultFolder, fileName);
            return virPath;
        }

        public static string ToRootRelativePath(string storedPath)
        {
            return "/" + VirtualPathUtility.MakeRelative("/", storedPath);
        }

        public static string NotAvailableImagePath()
        {
            return "~/Images/NA.jpg";
        }
    }
}
