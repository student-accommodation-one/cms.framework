﻿using Kara.CMS.Entities;
using Kara.CMS.Repository;
using Kara.Framework.Common;
using Kara.Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Business.Processing
{
    public class LookupProcessing
    {
        public ActionResultCustom<LuContentType> GetLuContentTypes(bool clearCache = false) 
        {
            try
            {
                var result = new ActionResultCustom<LuContentType>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new LuContentTypeRepository(unitOfWork);
                    var repositoryResult = repo.GetAllFromCache(clearCache);
                    result.Results.AddRange(repositoryResult);
                }

                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
