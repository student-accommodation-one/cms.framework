﻿using Kara.CMS.Common.Enums;
using Kara.CMS.Entities;
using Kara.CMS.Repository;
using Kara.Framework.Common;
using Kara.Framework.Common.Entity;
using Kara.Framework.Common.Enums;
using Kara.Framework.Common.Helpers;
using Kara.Framework.Common.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kara.CMS.Business.Processing
{
    public class ContentProcessing
    {
        public ActionResultCustom<Page> SavePages(IList<Page> Pages)
        {
            try
            {
                var result = new ActionResultCustom<Page>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new PageRepository(unitOfWork);
                    foreach (var item in Pages)
                        repo.Save(item, e => e.PageID);
                    unitOfWork.Save();
                }
                result.Results.AddRange(Pages);
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<Section> SaveSections(IList<Section> Sections)
        {
            try
            {
                var result = new ActionResultCustom<Section>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new SectionRepository(unitOfWork);
                    foreach (var item in Sections)
                        repo.Save(item, e => e.SectionID);
                    unitOfWork.Save();
                }
                result.Results.AddRange(Sections);
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public IList<Page> GetPagesAndSections(Expression<Func<Page, bool>> baseFilter)
        {
            try
            {
                var result = new List<Page>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new PageRepository(unitOfWork);
                    result = repo.Find(baseFilter, p => p.Sections);
                }

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public IList<Section> GetSections(Expression<Func<Section, bool>> baseFilter)
        {
            try
            {
                var result = new List<Section>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new SectionRepository(unitOfWork);
                    result = repo.Find(baseFilter, s => s.Page);
                }

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }
        public IList<Section> GetSectionAndWidgets(Expression<Func<Section, bool>> baseFilter)
        {
            try
            {
                var result = new List<Section>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new SectionRepository(unitOfWork);
                    result = repo.Find(baseFilter, c => c.WidgetGalleries);
                }

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }
        public ActionResultCustom<ContentItem> GetContentItems(Expression<Func<ContentItem, bool>> baseFilter, EntityDataRequest request)
        {
            try
            {
                var result = new ActionResultCustom<ContentItem>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new ContentItemRepository(unitOfWork);
                    result = repo.GetByPage(baseFilter, request);
                }

                result.IsSuccess = true;

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<VwPageSection> GetPageSectionView(Expression<Func<VwPageSection, bool>> baseFilter, EntityDataRequest request)
        {
            try
            {
                var result = new ActionResultCustom<VwPageSection>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new VwPageSectionRepository(unitOfWork);
                    result = repo.GetByPage(baseFilter, request);
                }

                result.IsSuccess = true;

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }
      
    }
}
