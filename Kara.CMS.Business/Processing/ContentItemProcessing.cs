﻿using Kara.CMS.Common.Enums;
using Kara.CMS.Entities;
using Kara.CMS.Repository;
using Kara.Framework.Common;
using Kara.Framework.Common.Enums;
using Kara.Framework.Common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kara.CMS.Business.Processing
{
    public class ContentItemProcessing
    {
        public ActionResultCustom<string> UpdateContentItems(List<ContentItem> contentItems)
        {
            var result = new ActionResultCustom<string>();
            using (var unitOfWork = new KaraCmsUnitOfWork())
            {
                var repo = new ContentItemRepository(unitOfWork);
                result = repo.UpdateContentItems(contentItems);
            }

            return result;
        }

        public ActionResultCustom<string> UploadContentItems(HttpFileCollectionBase files)
        {
            var result = new ActionResultCustom<string>();

            List<LuContentType> contentTypes;

            using (var unitOfWork = new KaraCmsUnitOfWork())
                contentTypes = new LuContentTypeRepository(unitOfWork).GetAllFromCache();

            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];

                var fileInfo = new FileInfo(file.FileName);
                MimeTypes fileContentType = MimeTypes.DEFAULT;
                if (Enum.TryParse<MimeTypes>(fileInfo.Extension.Replace(".", ""), true, out fileContentType))
                {
                    var category = fileContentType.ToValueCategory();
                    var contentType = contentTypes.First(c => c.Description.ToLower() == category);
                    var newItem = new ContentItem()
                    {
                        ContentTypeID = contentType.ContentTypeID,
                        OriginalFileName = fileInfo.Name,
                        Description = fileInfo.Name,
                        FileContentType = fileContentType.ToValue(),
                        Extension = fileContentType.ToString().ToLower(),
                        FileSize = file.ContentLength,
                        LastUpdatedBy = System.Threading.Thread.CurrentPrincipal.Identity.Name,
                        LastUpdatedDate = DateTime.UtcNow
                    };


                    try
                    {
                        using (var unitOfWork = new KaraCmsUnitOfWork())
                        {
                            var repo = new ContentItemRepository(unitOfWork);
                            repo.Save(newItem, c => c.ContentItemID);
                            unitOfWork.Save();
                        }

                        newItem.FileName = newItem.GenerateFileName();
                        
                        //Can put the file processing into transaction if required, keep it light for the moment, can use scheduled clean up
                        var virPath = WebPathSettings.GetContentItemVirtualPath(contentType, newItem, false);
                        newItem.Path = virPath;

                        var physicalPath = System.Web.HttpContext.Current.Server.MapPath(virPath);
                        var newFileInfo = new FileInfo(physicalPath);
                        if (!newFileInfo.Directory.Exists)
                            newFileInfo.Directory.Create();

                        file.SaveAs(newFileInfo.FullName);

                        string thumbnailVirtualPath = null;
                        if (newItem.ContentTypeID == (int)ContentTypes.IMAGE)
                        {
                            thumbnailVirtualPath = WebPathSettings.GetContentItemVirtualPath(contentType, newItem, true);
                            newItem.ThumbnailPath = thumbnailVirtualPath;
                            newItem.ThumbnailFileName = newItem.GenerateFileName(true);
                            var thumbnailPhysicalPath = System.Web.HttpContext.Current.Server.MapPath(thumbnailVirtualPath);

                            using (var thumbnail = ImageHelper.GetThumbnail(newFileInfo.FullName, 100))
                                thumbnail.Save(thumbnailPhysicalPath);
                        }

        
                        //Save paths
                        using (var unitOfWork = new KaraCmsUnitOfWork())
                        {
                            var repo = new ContentItemRepository(unitOfWork);
                            repo.Save(newItem, c => c.ContentItemID);
                            unitOfWork.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }


            result.IsSuccess = true;
            return result;
        }

        public ActionResultCustom<string> DeleteContentItems(List<ContentItem> models)
        {
            var result = new ActionResultCustom<string>();
            using (var unitOfWork = new KaraCmsUnitOfWork())
            {
                var repo = new ContentItemRepository(unitOfWork);
                foreach (var item in models)
                    repo.Delete(item);
                unitOfWork.Save();
            }
            //Can put the file processing into transaction if required, keep it light for the moment, can use scheduled clean up
            var contentType = new LookupProcessing().GetLuContentTypes().Results.First(l => l.ContentTypeID == (int)ContentTypes.IMAGE);

            foreach (var item in models)
            {
                var physicalPath = System.Web.HttpContext.Current.Server.MapPath(item.Path);
                var thumbnailVirtualPath = WebPathSettings.GetContentItemVirtualPath(contentType, item, true);
                var thumbnailPath = System.Web.HttpContext.Current.Server.MapPath(thumbnailVirtualPath);
                try
                {
                    if (File.Exists(physicalPath)) File.Delete(physicalPath);                  
                }
                catch (Exception)
                {
                    //File might be locked just leave it, suppress exception
                    //throw;
                }
                try
                {
                    if (File.Exists(thumbnailPath)) File.Delete(thumbnailPath);
                }
                catch (Exception)
                {
                    //File might be locked just leave it, suppress exception
                    //throw;
                }

               
            }
            result.IsSuccess = true;
            return result;
        }        
    }
}
