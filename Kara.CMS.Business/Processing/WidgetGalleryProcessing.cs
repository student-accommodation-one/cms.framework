﻿using Kara.CMS.Entities;
using Kara.CMS.Repository;
using Kara.Framework.Common;
using Kara.Framework.Common.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Business.Processing
{
    public class WidgetGalleryProcessing
    {
        public ActionResultCustom<VwWidgetGallery> GetWidgetGalleryView(Expression<Func<VwWidgetGallery, bool>> baseFilter, EntityDataRequest request)
        {
            try
            {
                var result = new ActionResultCustom<VwWidgetGallery>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new VwWidgetGalleryRepository(unitOfWork);
                    result = repo.GetByPage(baseFilter, request);
                }

                result.IsSuccess = true;

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<VwWidgetGalleryItem> GetWidgetGalleryItemsView(Expression<Func<VwWidgetGalleryItem, bool>> baseFilter, EntityDataRequest request)
        {
            try
            {
                var result = new ActionResultCustom<VwWidgetGalleryItem>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new VwWidgetGalleryItemRepository(unitOfWork);
                    result = repo.GetByPage(baseFilter, request);
                }

                result.IsSuccess = true;

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<WidgetGallery> GetWidgetGalleries(Expression<Func<WidgetGallery, bool>> baseFilter)
        {
            try
            {
                var result = new ActionResultCustom<WidgetGallery>();
                List<WidgetGallery> queryResult;

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new WidgetGalleryRepository(unitOfWork);
                    queryResult = repo.Find(baseFilter, g => g.Section);
                }

                result.IsSuccess = true;
                result.Results.AddRange(queryResult);

                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }        

        public ActionResultCustom<WidgetGallery> SaveWidgetGalleries(IList<WidgetGallery> widgetGalleries)
        {
            try
            {
                var result = new ActionResultCustom<WidgetGallery>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new WidgetGalleryRepository(unitOfWork);
                    foreach (var item in widgetGalleries)
                        repo.Save(item, e => e.WidgetGalleryID);
                    unitOfWork.Save();
                }
                result.Results.AddRange(widgetGalleries);
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<WidgetGalleryItem> SaveWidgetItems(IList<WidgetGalleryItem> widgetItems)
        {
            try
            {
                var result = new ActionResultCustom<WidgetGalleryItem>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new WidgetGalleryItemRepository(unitOfWork);
                    foreach (var item in widgetItems)
                        repo.Save(item, e => e.WidgetGalleryItemID);
                    unitOfWork.Save();
                }
                result.Results.AddRange(widgetItems);
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<VwWidgetGalleryItem> DeleteWidgetGalleryItems(IList<VwWidgetGalleryItem> widgetItems)
        {
            try
            {
                var result = new ActionResultCustom<VwWidgetGalleryItem>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new WidgetGalleryItemRepository(unitOfWork);
                    foreach (var item in widgetItems)
                        repo.Delete(item.WidgetGalleryItemID);
                    unitOfWork.Save();
                }
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }

        public ActionResultCustom<int> DeleteWidgetGallery(List<int> widgetGalleryIds)
        {
            try
            {
                var result = new ActionResultCustom<int>();

                using (var unitOfWork = new KaraCmsUnitOfWork())
                {
                    var repo = new WidgetGalleryRepository(unitOfWork);
                    foreach (var item in widgetGalleryIds)
                        repo.Delete(item);
                 
                    unitOfWork.Save();
                }
                result.Results = widgetGalleryIds;
                result.IsSuccess = true;
                return result;
            }
            catch (Exception ex)
            {
                //TODO: Loggging
                throw;
            }
        }
    }
}
