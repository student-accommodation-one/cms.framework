﻿using Kara.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Common.Enums
{
    public enum ContentTypes
    {
        [Code("NONE")]
        NONE = 0,
        [Code("HTML")]
        HTML = 1,
        [Code("TEXT")]
        TEXT = 11,
        [Code("IMAGE")]
        IMAGE = 200,
        [Code("APPLICATION")]
        APPLICATION = 300,
        [Code("VIDEO")]
        VIDEO = 400,
        [Code("AUDIO")]
        AUDIO = 500,
        [Code("WIDGET_GALLERY")]
        WIDGET_GALLERY = 600
    }
}
