﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kara.CMS.Common
{
    public class CmsIdFactory
    {
        public static string Create(string controllerFullName, string actionFullName)
        {
            return string.Format("{0}.{1}", controllerFullName, actionFullName);
        }
    }
}
