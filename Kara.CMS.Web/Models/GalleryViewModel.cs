﻿using Kara.CMS.Business;
using Kara.CMS.Business.Processing;
using Kara.CMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kara.CMS.Web.Models
{
    public class GalleryViewModel
    {
        public List<VwWidgetGalleryItem> GalleryItems { get; set; }
        public List<IGrouping<int, VwWidgetGalleryItem>> FolderItems { get; set; }

        public GalleryViewModel()
        {
            GalleryItems = new List<VwWidgetGalleryItem>();
        }
        public GalleryViewModel(Section section, int folderId)
            : this()
        {

            var processing = new WidgetGalleryProcessing();
            var results = processing.GetWidgetGalleryItemsView(i => i.SectionID == section.SectionID, new Framework.Common.Entity.EntityDataRequest());
            FolderItems = results.Results.GroupBy(g => g.WidgetGalleryID).ToList();

            var folders = results.Results.Select(s => s.WidgetGalleryID).Distinct().OrderByDescending(i => i);
            if (folders.Any())
            {
                if (folderId == 0) folderId = folders.Last();
                var itemsToDisplay = results.Results.Where(f => f.WidgetGalleryID == folderId).ToList();
                foreach (var item in itemsToDisplay)
                {
                    item.Path = WebPathSettings.ToRootRelativePath(item.Path);
                    item.ThumbnailPath = WebPathSettings.ToRootRelativePath(item.ThumbnailPath);
                }
                GalleryItems = itemsToDisplay;
            }
        }

    }
}