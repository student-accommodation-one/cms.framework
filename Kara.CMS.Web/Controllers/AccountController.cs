﻿using Kara.CMS.Web.Common.Controllers;
using Kara.CMS.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Kara.CMS.Web.Controllers
{


    public class AccountController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Logon(LoginViewModel model)
        {
            if (FormsAuthentication.Authenticate(model.Email, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.Email, false);
                return RedirectToAction("Index", "Cms");
            }
            else
            {
                ViewData["LastLoginFailed"] = true;
                return Login();
            }
        }
    }
}