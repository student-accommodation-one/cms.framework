﻿using Kara.CMS.Web.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kara.CMS.Web.Controllers
{
    [AllowAnonymous]
    public class GalleryController : BaseController
    {
        // GET: Gallery
        public ActionResult Index(int? id)
        {
            ViewData["FolderId"] = id;
            return View();
        }
    }
}