﻿/* History v1.4 
 * Added Map
 */
var Global = {};
Global.Settings = {};
Global.Settings.ModalElementId = "#modalWindow";
Global.Settings.ServerUrl = "/Kara.CMS.Web";
Global.Map = {};
Global.Map.MapPoints = {};
Global.initAjax = function () {
    $.ajaxSetup({
        cache: false
    });

    $(document)
    .ajaxStart(function (e) {

    })
    .ajaxSuccess(function (e, xhr, settings) {
        if (settings.dataType != 'json') {
            //
        }
    })
    .ajaxError(function (e, xhr, settings, exception) {
        if (exception == 'abort') return;
        alert(exception);
    })
    .ajaxComplete(function (e, xhr, settings) {

    })
    .ajaxStop(function (e) {

    });
    $.ajaxPrefilter(function (settings, originalSettings, jqXhr) {

    });
}

Global.DisplayMessage = function (message, msgType) {

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "500",
        "timeOut": "2500",
        "extendedTimeOut": "2000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    switch (msgType) {
        case "info":
            toastr.options.timeOut = "8000";
            break;
        default:
    };

    toastr[msgType](message);
};

Global.showNotifications = function () {
    if ($('#NotificationSuccess').val()) {
        Global.DisplayMessage($('#NotificationSuccess').val(), 'success');
    }
    if ($('#NotificationInfo').val()) {
        Global.DisplayMessage($('#NotificationInfo').val(), 'info');
    }
    if ($('#NotificationWarning').val()) {
        Global.DisplayMessage($('#NotificationWarning').val(), 'warning');
    }
    if ($('#NotificationError').val()) {
        Global.DisplayMessage($('#NotificationError').val(), 'error');
    }
    $('#NotificationSuccess').val('');
    $('#NotificationWarning').val('');
    $('#NotificationInfo').val('');
    $('#NotificationError').val('');
};

Global.clearNotifications = function () {
    $('#NotificationSuccess').remove();
    $('#NotificationWarning').remove();
    $('#NotificationInfo').remove();
    $('#NotificationError').remove();
};

Global.showModalWindow = function (title, contentUrl, width, height) {
    $(Global.Settings.ModalElementId).html("");
    var wnd = $(Global.Settings.ModalElementId).data("kendoWindow");
    if (height == null || height == "0") height = "600";
    if (width == null || width == "0") width = "480";
    if (!wnd) {
        $(Global.Settings.ModalElementId).kendoWindow({
            actions: ["Maximize", "Close"],
            draggable: true,
            visible: false,
            height: height,
            modal: true,
            resizable: true,
            width: width,
            iframe: false
        }).data("kendoWindow");
    }
    var window = $(Global.Settings.ModalElementId).data("kendoWindow");
    window.setOptions({
        width: width,
        height: height
    });
    window.title(title);
    window.refresh({ type: "GET", url: contentUrl }).center().open();
};

Global.hideModalWindow = function () {
    var window = $(Global.Settings.ModalElementId).data("kendoWindow");
    window.close();
}

Global.initJSControls = function () {
    $('[data-karatype="JSDatePicker"]').livequery(function () {
        var $this = $(this); $this.kendoDatePicker({});
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="JSTimePicker"]').livequery(function () {
        var $this = $(this); $this.kendoTimePicker({ interval: 15 });
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="JSDropDownList"]').livequery(function () {
        var $this = $(this); $this.kendoDropDownList({});
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="JSNumeric"]').livequery(function () {
        var $this = $(this);
        $(this).kendoNumericTextBox({ decimals: 0, min: 0, format: "#" });
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="JSDecimal"]').livequery(function () {
        var $this = $(this);
        $(this).kendoNumericTextBox({ decimals: 2, min: 0 });
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="JSMoney"]').livequery(function () {
        var $this = $(this);
        $(this).kendoNumericTextBox({ format: "c" });
        if ($this[0].initControl) $this[0].initControl();
    });
    $('[data-karatype="popover"]').livequery(function () {
        var $this = $(this);
        $(this).popover({ html: true });
        $this.css('cursor', 'pointer');
    });
}


Global.init = function () {
    Global.initAjax();
    Global.showNotifications();
    Global.initJSControls();
}

$(function () {
    Global.init();
});


(function ($) {

    $.fn.clearFormValidation = function () {
        this.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid').empty();
        this.find('.input-validation-error').removeClass('input-validation-error').addClass('valid');
        this.find('.control-group.error').removeClass("error");
        return this;
    };

}(jQuery));

Global.parseFormValidation = function (formId) {
    var form = $(formId);
    if (form) {
        form.removeData("validator");
        form.removeData("unobtrusiveValidation");
        form.removeAttr("novalidate");
        $.validator.unobtrusive.parse(form);
    }

}

Global.submitAjaxForm = function (form, formPlaceholderId, successPlaceholderId) {
    var $form = $(form);
    if ($form.valid()) {
        var postData = $form.serializeArray();
        var formURL = $form.attr("action");
        $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                if (data.indexOf("hideModalWindow") < 0)
                    $(formPlaceholderId).html(data);
                else {
                    $(successPlaceholderId).empty();
                    $(successPlaceholderId).html(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //if fails
                alert(errorThrown);
            }
        });
    }
}

Global.setEnableInput = function (el, enable) {
    if (enable)
        el.removeAttr("readonly");
    else
        el.attr("readonly", "readonly");
}

Global.setEnableJSDropDownList = function (el, enable) {
    var tp = el.data('kendoDropDownList');
    if (tp) {
        if (enable)
            tp.readonly(false);
        else
            tp.readonly();
    }
}

Global.setEnableJSTimePicker = function (el, enable) {
    var tp = el.data('kendoTimePicker');
    if (tp) {
        if (enable)
            tp.readonly(false);
        else
            tp.readonly();
    }
}

Global.setEnableJSDatePicker = function (el, enable) {
    var tp = el.data('kendoDatePicker');
    if (tp) {
        if (enable)
            tp.readonly(false);
        else
            tp.readonly();
    }
}

Global.Map.Initialize = function (canvas, image) {

    var map_canvas = document.getElementById(canvas);
    var map_options = {
        center: new google.maps.LatLng(-6.169175, 106.771526),
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    Global.Map.Map = new google.maps.Map(map_canvas, map_options)

    for (var i = 0; i < Global.Map.MapPoints.length; i++) {
        var point = Global.Map.MapPoints[i];
        var pos = new google.maps.LatLng(point.x, point.y);
        var pointImage;
        if (point.image == null) pointImage = image;
        var mkr = new google.maps.Marker({
            position: pos,
            map: Global.Map.Map,
            title: point.title,
            icon: pointImage
        });
        mkr.setMap(Global.Map.Map);
        Global.Map.AttachPointToHandler(mkr, point);
    }

}
Global.Map.MapPoint = function (title, x, y, image) {
    this.title = title;
    this.x = x;
    this.y = y;
    this.image = image;
}
Global.Map.RegisterPointLink = function (link) {
    var point = Global.Map.MapPoints[link - 1];
    Global.Map.AttachPointToHandler(document.getElementById("lnkMapPoint" + link), point);
}
Global.Map.ZoomTo = function () {
    if (Global.Map.ZoomFluid == 16) return 0;
    else {
        Global.Map.ZoomFluid++;
        Global.Map.Map.setZoom(Global.Map.ZoomFluid);
        setTimeout("Global.Map.ZoomTo()", 70);
    }
}
Global.Map.AttachPointToHandler = function (el, pos) {
    google.maps.event.addDomListener(el, "click", function (ev) {
        Global.Map.ZoomFluid = 11;
        Global.Map.ZoomCoords = new google.maps.LatLng(pos.x, pos.y);
        Global.Map.Map.panTo(Global.Map.ZoomCoords);
        Global.Map.ZoomTo();
    });
}