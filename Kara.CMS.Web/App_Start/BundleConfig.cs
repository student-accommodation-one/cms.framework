﻿using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace Kara.CMS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/headScripts").Include(
               "~/Scripts/jquery-{version}.js",
               "~/Scripts/jquery.livequery.min.js",
               "~/Scripts/jquery.validate*",
               "~/Scripts/kendo.all.min.js",
               string.Format("~/Scripts/cultures/kendo.culture.{0}.min.js", ConfigurationManager.AppSettings["JsCulture"]),
               "~/Scripts/toastr.js",
               "~/Scripts/kara.global.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/lateScripts").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                   "~/Content/bootstrap.css",
                   "~/Content/kendo.common.min.css",
                   "~/Content/kendo.silver.min.css",
                   "~/Content/site.css",
                   "~/Content/toastr.css"
                   ));

            bundles.Add(new StyleBundle("~/Content/cssCms").Include(
                       "~/Content/bootstrap.css",
                       "~/Content/kendo.common.min.css",
                       "~/Content/kendo.silver.min.css",
                       "~/Content/toastr.css"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/gallery").Include(
                  "~/Scripts/jssor.slider.min.js"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
