﻿using Kara.CMS.Entities;
using Kara.CMS.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Web.Mvc;

namespace Kara.CMS.Web
{
    public class AutoMapperConfig
    {
        public static void RegisterAllMaps()
        {
            Mapper.CreateMap<ContentItem, ContentItemViewModel>();
            Mapper.CreateMap<ContentItemViewModel, ContentItem>();
            Mapper.CreateMap<Section, SectionDetailsViewModel>()
                .ForMember(d => d.PageName, opt => opt.MapFrom(s => s.Page.Name))
                .ForMember(d => d.SectionName, opt => opt.MapFrom(s => s.Name));
          
            RegisterSelectItemMappings();
        }

        private static void RegisterSelectItemMappings()
        {
            Mapper.CreateMap<LuContentType, SelectListItem>()
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Description))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.ContentTypeID));

        }
    }
}