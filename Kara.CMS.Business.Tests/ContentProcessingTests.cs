﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kara.CMS.Entities;
using Kara.CMS.Business.Processing;
using Kara.CMS.Common;
using Kara.CMS.Common.Enums;
using System.Collections.Generic;

namespace Kara.CMS.Business.Tests
{
    [TestClass]
    public class ContentProcessingTests
    {
        [TestMethod]
        public void AddPageAndSectionTests()
        {
            var page = AddPageTest();

            AddSectionToPageTest(page);

            LoadThePageAndSection(page.CmsID);
        }

        private void LoadThePageAndSection(string cmsId)
        {
            var page = new ContentProcessing().GetPagesAndSections(p => p.CmsID == cmsId).First();


            Assert.AreNotEqual(0, page.Sections.Count, "Page and sections fail to load");
        }

        private void AddSectionToPageTest(Page page)
        {
            var methodInfo = System.Reflection.MethodInfo.GetCurrentMethod();
            var cmsId = CmsIdFactory.Create(this.ToString(), methodInfo.Name + DateTime.Now.Ticks);
            var section = new Section { CmsID = cmsId, ContentTypeID = (int)ContentTypes.HTML, PageID = page.PageID, Name = "Section Name" };
            section.Content = "<h5>Test" + page.CmsID + "</h5>";
            new ContentProcessing().SaveSections(new List<Section>() { section });

            Assert.AreNotEqual(0, section.SectionID, "Failed to add section");
        }

        private Page AddPageTest()
        {
            var methodInfo = System.Reflection.MethodInfo.GetCurrentMethod();
            var cmsId = CmsIdFactory.Create(this.ToString(), methodInfo.Name + DateTime.Now.Ticks);
            var page = new Page { CmsID = cmsId, Name = "Index Page", LastUpdatedBy = "Unit test", LastUpdatedDate = DateTime.UtcNow };
            new ContentProcessing().SavePages(new List<Page>(){ page });

            Assert.AreNotEqual(0, page.PageID, "Failed to add page");
            return page;
        }

    }
}
